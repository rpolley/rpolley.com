# set and install packages
FROM ruby:2.7-buster
RUN mkdir /rpolley.com
WORKDIR /rpolley.com
COPY build.sh /rpolley.com/build.sh
RUN chmod +x /rpolley.com/build.sh
RUN /rpolley.com/build.sh
COPY Gemfile /rpolley.com/Gemfile
RUN touch /rpolley.com/Gemfile.lock
RUN bundle install
COPY . /rpolley.com
RUN yarn install --check-files

# Tell rails to use port 80, and expose port 80
ENV PORT="80"
EXPOSE 80

ENV RAILS_SERVE_STATIC_FILES="True"
RUN rails assets:precompile

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
